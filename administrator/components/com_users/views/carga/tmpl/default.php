<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addStyleSheet($host.'administrator/components/com_users/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_users/assets/css/style.css');

$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/libs/fileuploader/fileuploader.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/misc/misc.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/views/tecnico.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/models/tecnico.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/controllers/tecnico.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/handlers/tecnico.js');
$document->addScript( $host.'administrator/components/com_users/assets/js/misc/file.js');
?>
<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
	<fieldset>
		<legend>Carga de Excel</legend>

		<p>Asegúrese que su archivo de Excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán.</p>
		<ul class="excel-list">
		</ul>
		<div id="excel-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>

		<p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
		<button href="#" id="importar-datos" class="importar-datos">Importar datos</button>

		<!-- <p class="truncate-label"></p> -->
		<ul class="excel-parse-list"></ul>
		

		<div class="excel-reporte">
			<h3>Reporte</h3>
			<ul>
				<li>Errores en la conversión: <span class="excel-errors">0</span></li>
			</ul>
			<div class="excel-log">
			</div>
		</div>

	</fieldset>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>

</form>