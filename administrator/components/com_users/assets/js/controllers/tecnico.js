/**
*
* Controller for { Tecnico }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var TecnicoController = function( a ){
		
		// atributes, selector or global vars
		this.excel = '';
		this.pdf = '';
		this.excels = [];
		
	};
	
	TecnicoController.prototype ={
			

			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
			parseExcel: function( data ){

				view.onBeforeParse( this.excels );
				this.recursiveParse( 0, data );

				// For each file in excels array, make a request to save the excel data
				// for (var i = 0; i < this.excels.length ; i++ ) 
				// }
				
			}

		,	recursiveParse: function( index, data ){

				var	_this = this
				,	_data = data;

				if( index < this.excels.length ){

					data.excel = this.excels[index];

					var excelName = data.excel.replace( /.xls/g, '' ).replace( /.xls/g, '' ).replace( / /g, '-' );
					$( '#' + excelName ).html( excelName + ': Convirtiendo' + view.loader );

					var success = function( data ){

						console.log( data );

						view.onCompleteParse( data );

						//count--;

						index++;

						_this.recursiveParse( index, _data );
					};

					var error = function ( XMLHttpRequest, textStatus, errorThrown ) {
                        view.onError( XMLHttpRequest, textStatus, errorThrown, excelName );

                        index++;

						_this.recursiveParse( index, _data );  
                    };

					model.parseExcel( success, error,  data );

					

				} else {

					_this.deleteTemp( data );
				}
			}

			/**
			* Deletes the uploaded file
			*
			*/
		,	deleteTemp: function( count, data ){

				// At the end delete the excels file
				if( count <= 0 ){
					
					view.onBeforeDeleteTemp();

					var completeDeleteTemp = function( data ){

						view.onCompleteDeleteTemp( data );
					};

					return model.deleteTemp( completeDeleteTemp, data  );
				}
			}

		,	truncate: function( _data ){

				view.onBeforeTruncate();

				var _this = this;

				var success = function( data ){

					view.onCompleteTruncate( data );

					if( data.status == 200 )
						_this.parseExcel( _data );
				};

				return model.truncate( success, _data );
			}


			/**
			* Verifies the pdf file with some data
			*
			*/
		,	verifyMatricula: function( fileName ){

				var success = function( data ){

					view.renderPDFLog( data );
				};

				return model.verifyMatricula( success, fileName );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.TecnicoController = new TecnicoController();
	
})( jQuery, this, this.document, this.TecnicoModel, this.TecnicoView, this.Misc, undefined );