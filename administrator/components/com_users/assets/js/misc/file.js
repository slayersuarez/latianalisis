/**
*
* File uploading
*
*/

( function( $, window, document, tecnico ){

	var Uploader = function( a ){

		this.Excelparams = {

				element: document.getElementById( 'excel-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_financiero',
			    	task: 'carga.uploadExcel'
			    }

			,   allowedExtensions: [ 'xls' ]
			,   debug: false
			,	onSubmit: this.onSubmitExcel
			,	onProgress: this.onProgressExcel
			,	onComplete: this.onCompleteExcel
			,   onError: this.onErrorExcel
		    
		};


		this.PDFparams = {

				element: document.getElementById( 'pdf-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_carga',
			    	task: 'carga.uploadPdfs'
			    }

			,   allowedExtensions: [ 'pdf' ]
			,   debug: false
			,	onSubmit: this.onSubmitPDF
			,	onProgress: this.onProgressPDF
			,	onComplete: this.onCompletePDF
			,   onError: this.onErrorPDF
		    
		};

		this.pdfQueue = 0;

		this.pdfUploadCount = 1;

		this.fileList = 0;

	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploaderExcel();
				this.createUploaderPDF();
			}

		,	createUploaderExcel: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.Excelparams );

			}

		,	onSubmitExcel: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				var htmlName = fileName.replace( /.xls/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				console.log(htmlName);

				i.addClass( 'icon-excel' );
				spanName.text( fileName );
				spanName.addClass('filename');
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.attr( 'id', 'item-' + htmlName );

				li.appendTo( $( '.excel-list' ) );
			}

		,	onProgressExcel: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				var htmlName = fileName.replace( /.xls/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				$( '#item-' + htmlName + ' .percentage' ).text( perc + '%' );
				$( '#item-' + htmlName + ' .progress-bar' ).width( perc + 'px' );

			}

		,	onCompleteExcel: function(id, fileName, responseJSON){

				$('#importar-datos').animate({'opacity':'1'});
				$('.importar-datos').css({'display':'block'});

				$('.import-label').show('slow');

				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				// Save the file name in array to be processed at the end
				tecnico.excels.push( fileName );

			}

		,   onErrorExcel: function(id, fileName, xhr){
				console.log(xhr);
				var span = $('<span>');
				span.addClass('error');
				span.text( 'Falló subida de este archivo' );
				$('#item-' + fileName.replace( /.xls/g, '') ).append( span );
				return;
			}

		,	createUploaderPDF: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.PDFparams );

			}

		,	onSubmitPDF: function( id, fileName ){

				// Reset all strings
				$( '.pdf-upload-status' ).find( '.message' ).text( '' );
				$( '.pdf-total-title' ).text( '' );
				$( '.pdf-status-bar' ).find( '.bar-inside' ).css( 'width', '0%' );
				$( '.pdf-total-bar' ).find( '.bar-inside' ).css( 'width', '0%' );

				window.Uploader.pdfQueue = 0;
				window.Uploader.pdfUploadCount = 1;

				var queue = $( "input[name='file']" )[1].files;

				if( window.Uploader.pdfQueue == 0 ){
					
					window.Uploader.pdfQueue = queue.length;

					$( '.pdf-upload-status' ).find( '.message' ).text(
						'Subiendo PDFs 1 de ' + queue.length 
					);

				}

				$( '.pdf-upload-status-list' ).show("slow");

			}

		,	onProgressPDF: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				var progressBar = $( '.pdf-status-bar' ).find( '.bar-inside' )
				,	progressMessage = $( '.pdf-status-bar' ).find( '.bar-message' );

				progressBar.css( 'width', perc + '%' );
				progressMessage.text( fileName + ' (' + perc + '%)' );

			}

		,	onCompletePDF: function(id, fileName, responseJSON){

				var count = window.Uploader.pdfUploadCount++;
				var perc = Math.floor( count / window.Uploader.pdfQueue * 100 );

				var totalTitle = $( '.pdf-total-title' )
				,	progressBar = $( '.pdf-total-bar' ).find( '.bar-inside' )
				,	progressMessage = $( '.pdf-total-bar' ).find( '.bar-message' );

				progressBar.css( 'width', perc + '%' );
				progressMessage.text( '(' + perc + '%)' );
				totalTitle.text( 'Total (' + count + '/' + window.Uploader.pdfQueue + ')' );

				//window.TecnicoController.verifyMatricula( fileName );

				$( '.pdf-upload-status' ).find( '.message' ).text(
					'Subiendo matrículas ' + count + ' de ' + window.Uploader.pdfQueue 
				);

				if( perc == 100 ){
					progressBar.addClass('success');
					return;
				}
			}

		,   onErrorPDF: function(id, fileName, xhr){
				
				var li = $('<li>');

				li.text( 'Error: Archivo' + fileName + ' no cargado correctamente.'  );

				li.appendTo('.pdf-error-list');

				$('.pdf-error-list').show();
			}
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, this.TecnicoController, undefined );







