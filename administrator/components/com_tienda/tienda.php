<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );

$controller = JControllerLegacy::getInstance('tienda');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>