<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/PHPExcel.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/PHPExcel/IOFactory.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/uploader.php' );


$controller = JControllerLegacy::getInstance('financiero');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>