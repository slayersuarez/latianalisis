<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();

$document->addStyleSheet($host.'administrator/components/com_financiero/assets/css/style.css');


?>

<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
	<div class="clr"> </div>

	<fieldset class="add-variable">
		<legend>Añadir nueva variable</legend>

		<ul>
			<li><label>Nombre Variable: </label><input type="text" name="nombre" value="<?= $this->variable->nombre ?>"/></li>
			<li><label>Valor: </label><input type="text" name="valor" placeholder="Ej: 10.20" value="<?= $this->variable->valor ?>"/><span>%</span></li>
		</ul>

	</fieldset>

	

	<div>
		<input type="hidden" name="id" value="<?= $this->variable->id ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>