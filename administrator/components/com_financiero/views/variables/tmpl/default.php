<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();

?>

<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th width="10%" class="center">
					Id
				</th>
				<th class="center">
					Nombre
				</th>
				<th class="nowrap" width="10%">
					Valor
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="4">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php 

				foreach ($this->variables as $key => $variable) {
				?>

				<tr>
					<td>
						<?php echo JHtml::_('grid.id', $key, $variable->id); ?>
					</td>
					<td class="center"><?= $variable->id ?></td>
					<td class="center">
						<?= $variable->nombre ?>
					</td>
					<td class="center">
						<?= $variable->valor ?>
					</td>
				</tr>
				<?php
				}

			?>

		</tbody>
	</table>


	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>