<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class FinancieroViewVariables extends JViewLegacy {

	public $variables;
	public $variable;

	
	// Function that initializes the view
	function display( $tpl = null ){

		$this->variables = $this->get('Objects');
	
		// Add the toolbar with the actions
		$this->addToolbar();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::title( "Administrar Variables de Fórmula", 'cpanel' );

	

		$layout = $this->getLayout();

		if ($layout != 'default') {
			JToolBarHelper::save( "variables.save" );
			JToolBarHelper::divider();
			JToolBarHelper::cancel( "variables.cancel" );

		}

		if ($layout == 'default') {
			// JToolBarHelper::addNew( "variables.nuevo" );
			JToolBarHelper::divider();
			JToolBarHelper::editList( "variables.editar" );
		}
		
	}

}
?>