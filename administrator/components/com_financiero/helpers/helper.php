<?php
/**
 * Helper class for anuncios component
 *
 * 
 */
class Carga {

    public static function getSedes()  {

		$db = JFactory::getDbo();

    	$query = $db->getQuery(true);
    	$query->select('id as value, nombre as text');
    	$query->from('#__sedes');
    	
    	$db->setQuery( $query );

    	return $db->loadObjectList();
    }


}
?>