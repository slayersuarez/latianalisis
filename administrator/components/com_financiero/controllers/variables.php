
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class FinancieroControllerVariables extends JControllerLegacy{


	public function nuevo(){


		$view = $this->getView( 'variables', 'html' );

		$view->setLayout( 'new' );

		$view->display();
	}

	public function editar(){

		$variablesModel = $this->getModel('variables');

		$id = JRequest::getVar('cid');

		$variables = $variablesModel->getObject( $id[0] );

		$view = $this->getView( 'variables', 'html' );

		$view->assignRef( 'variable', $variables );

		$view->setLayout( 'new' );

		$view->display();
	}

	public function save(){

		$app = JFactory::getApplication();
		$input = $app->input;

		// Get the post from data
		$data = $input->getArray( $_POST );

		if (  empty( $data['nombre'] ) )
			$app->redirect('index.php?option=com_financiero&view=variables&layout=new', 'Digite el nombre de la variable', 'notice');

		if (  empty( $data['valor'] ) )
			$app->redirect('index.php?option=com_financiero&view=variables&layout=new', 'Digite el valor de la variable', 'notice');

		$variablesModel = $this->getModel('variables');

		$variablesModel->instance( $data );

		if ( ! $variablesModel->save( 'bool' ) ) {
			$app->redirect( 'index.php?option=com_financiero&view=variables&layout=new', 'No se pudo guardar la variable.', false );
		}

		$app->redirect( 'index.php?option=com_financiero&view=variables', 'Variable guardada correctamente.', false );
	}

	public function cancel(){

		$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_financiero&view=variables', 'Operación cancelada', 'error');

	}


}
?>