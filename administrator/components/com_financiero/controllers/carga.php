
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class FinancieroControllerCarga extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xls' );
		// max file size in bytes
		$sizeLimit = 32 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}

	/**
	*
	* Uploads pdf files
	*
	*/
	public function uploadPdfs(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'pdf' );
		// max file size in bytes
		$sizeLimit = 32 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../pdfs/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}


	/**
	* Parse and excel file and save it into database
	*
	*/
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );
		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];
		// // get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

		//$response->log .= 'Archivo encontrado, dispuesto a parsearse...<br>';
		

		// Parse the excel file			
		try {
			$file = PHPExcel_IOFactory::load( $inputFileName );

		} catch(Exception $e) {

			$response->status = 500;
			$response->excel = $data['excel'];
			$response->message = 'Error loading file '.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage();
			
			// delete the file
			unlink( $inputFileName );
			
			echo json_encode( $response );
			die();
		}
		
		$sheetData = $file->getActiveSheet()->toArray(null,true,true,true);


		$response = $this->save( $sheetData, $response );
		$response->excel = $data['excel'];

		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	*
	* Saves a style sheet in the db
	*/
	public function save( $data, $response ){


		if( ! is_array( $data ) )
			return false;

		// Instance the model
		$model = $this->getModel( 'usuarios' );

		// Delete the null cells
		$sheet = array();

		$sheet = $data;

		$sheet[ 1 ] = array_filter( $sheet[ 1 ] );



		// Ensure the model has the same number of attributes than data keys
		// $model_keys_count = count( $model->attrs_map ) - 1; // less the id
		// $sheet_keys_count = count( $sheet[1] );


		// $response->modelCount = $model_keys_count;
		// $response->sheetCount = $sheet_keys_count;

		// if( $model_keys_count != $sheet_keys_count ){

		// 	$response->status = 500;
		// 	$response->message = 'El archivo no se pudo convertir debido a que no coincide con el formato. Asegúrese que el archivo tiene el formato correcto, el mismo número de columnas.';

		// 	return $response;
		// }

		$rows = array();


		// // set the args for each row and save in db
		for( $i = 2; $i <= count( $sheet ); $i++ ){ 

			$row = $sheet[ $i ];

			$block = 0;
			$estado = 'activo';

			if ($row['I'] == 'no') {
				$block = 1;
				$estado = 'inactivo';
			}

			$_model = $this->getModel( 'usuarios' );

			//generate password
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';

		    for ($a = 0; $a < 32; $a++) {
		        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
		    }

		    // generates joomla password
		   	$pass = md5( '1234' . $randomString ) . ':' . $randomString;

		   	// date of register
		   	$date = date( 'Y-m-d H:i:s' );

		   	$empresas = ($row['K'] == 'si') ? 1 : 0;
		   	$avanzado = ($row['L'] == 'si') ? 1 : 0;
		   	$sectorial = ($row['M'] == 'si') ? 1 : 0;

		   	$usuario = $_model->getUser( $row['C'] );

		   	if (! empty( $usuario )) {

				$args = array(
						'id' => $usuario->id
					,	'name' => $row['A']
					,	'identificacion' => $row['H']
					,	'direccion' => $row['E']
					,	'ciudad' => $row['F']
					,	'pais' => $row['G']
					,	'telefono' => $row['D']
					,	'celular' => $row['J']
					,	'apellidos' => $row['B']
					,	'tipo' => 'antiguo'
					,	'block' => $block
					,	'empresas' => $empresas
					,	'avanzado' => $avanzado
					,	'sectorial' => $sectorial
				);

		   	}else{
		   		$args = array(
						'name' => $row['A']
					,	'username' => $row['C']
					,	'password' => $pass
					,	'email' => $row['C']
					,	'identificacion' => $row['H']
					,	'direccion' => $row['E']
					,	'ciudad' => $row['F']
					,	'registerDate' => $date
					,	'pais' => $row['G']
					,	'telefono' => $row['D']
					,	'celular' => $row['J']
					,	'apellidos' => $row['B']
					,	'tipo' => 'nuevo'
					,	'block' => $block
					,	'empresas' => $empresas
					,	'avanzado' => $avanzado
					,	'sectorial' => $sectorial
				);

		   	}


			// Instance the model
			$_model->instance( $args );

			// save row
			if( ! $_model->save( 'bool' ) ){
				$response->log .= 'Usuario en la ila ' . $i . 'no se pudo guardar<br>';	
				continue;
			}

			$id = ($_model->insertId == null) ? $usuario->id : $_model->insertId;

		    // merge group registered with user
		    $groupArgs = array('user_id' => $id , 'group_id' => '2' );

		    $groupModel = $this->getModel('groups');
		    $groupModel->instance( $groupArgs );
		    $groupModel->saveGroup();
				
			$response->log .= 'Usuario '.$i.' - '. $row['A'].' '.$row['B'].' se ha guardado correctamente, su estado es '.$estado.'.<br>';

			if ( empty( $usuario ) ) {
				$this->sendMail( $randomString, $row['C'] );
			}							
		}

		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
		

	}



	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		// $model = $this->getModel( 'usuarios' );

		//$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}

	public function sendMail( $pass, $user ){
		
		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/logo.png'><br><br>Le damos la bienvenida al portal de análisis financiero y de riesgo LATIANALISIS.COM. Su registro ha sido creado con éxito. A continación encontrará su usuario y contraseña: <br><br>";

        $contenido .= "Usuario:".$user."<br>";
        $contenido .= "Contraseña".$pass."<br><br>";
        $contenido .= "Siga el siguiente enlace para ingresar y, si lo desea cambiar su contraseña.<br>";
        $contenido .= "<a href='www.latianalisis.com'>www.latianalisis.com</a> <br><br>";
        $contenido .= "<p style='color: red;'>Si tiene incovenientes para el ingreso, por favor puede escribirnos a </p><a href='mailto:soporte@latianalisis.com'>soporte@latianalisis.com</a> <br><br>";
        $contenido .= "Si usted no ha creado o registrado este usuario y recibió este correo por error, por favor reportarlo a los siguientes correos electrónicos: <a href='mailto:soporte@latianalisis.com'>soporte@latianalisis.com</a> y <a href='mailto:contacto@latianalisis.com'>contacto@latianalisis.com</a><br><br>";
        $contenido .= "Cordialmente, <br>";
        $contenido .= "Latianalisis.<br><br>";
        $contenido .= "</div>";

        $mail->setSender( 'soporte@latianalisis.com' );
        $mail->addRecipient( $pass );
        $mail->setSubject( "Usted ha sido registrado en Latianalis.com" );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

        // enviar email
        $mail->Send();

	}

}
?>