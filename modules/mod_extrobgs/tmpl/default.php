<?php

/*
# -------------------------------------------------------------------------
# mod_extrobgs - eXtro Background Slider Module (E.R.B.S.E.)
# -------------------------------------------------------------------------
# author     eXtro-media.de
# copyright  Copyright (C) 2012 eXtro-media.de. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
# Websites:  http://www.eXtro-media.de
# Technical Support:  Forum - http://www.extro-media.de/en/forum.html
# -------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;

$effekt = $params->get('effekt','original');
$patternimg = $params->get('patternimg');
$duration = $params->get('durslide');
$slidezoom = $params->get('slidezoom');
$slidetilt = $params->get('slidetilt');
$classuffix = $params->get('classuffix');
$hgfarbe = $params->get('hgfarbe');
$titelfont = $params->get('titelfont');
$titelfontover = $params->get('titelfontover');
$titelfontsize = $params->get('titelfontsize');
$titlecolor = $params->get('titelfarbe');
$titeleffekt = $params->get('titeleffekt');
$basepath = JURI::base(true);
if($titelfontover != '') { $titelfont = $titelfontover; }

/*
$slideimg[] = $params->get('slide1img');
$slideimg[] = $params->get('slide2img');
$slideimg[] = $params->get('slide3img');
$slideimg[] = $params->get('slide4img');
$slideimg[] = $params->get('slide5img');
$slideimg[] = $params->get('slide6img');

$slidetext[] = $params->get('slide1txt');
$slidetext[] = $params->get('slide2txt');
$slidetext[] = $params->get('slide3txt');
$slidetext[] = $params->get('slide4txt');
$slidetext[] = $params->get('slide5txt');
$slidetext[] = $params->get('slide6txt');
*/

for($x=0;$x<50;$x++) {
  if ($params->get('slide'.($x + 1).'img') != '') { $slideimg[] = $params->get('slide'.($x + 1).'img'); $slidetext[] = $params->get('slide'.($x + 1).'txt'); }
}

$pattern = '';
if($patternimg != '') {
  if(substr($patternimg,0,4) == 'http') { $pattern = $patternimg; } elseif(substr($patternimg,0,1) == '/') { $pattern = $basepath.$patternimg; } else { $pattern = $basepath.'/'.$patternimg; }
}

$si = array();
$st = array();

for($x=0;$x<count($slideimg);++$x) {
  if(isset($slideimg[$x])) { 
    if(substr($slideimg[$x],0,4) == 'http') { $si[] = $slideimg[$x]; } elseif(substr($slideimg[$x],0,1) == '/') { $si[] = $basepath.$slideimg[$x]; } else { $si[] = $basepath.'/'.$slideimg[$x]; } 
    $st[] = $slidetext[$x];
  }
}

$document = JFactory::getDocument();

if($titelfont != 'none') :
  $document->addStyleSheet('http://fonts.googleapis.com/css?family='.$titelfont);
endif;

if($effekt == 'original') {

$anzsli = count($si);
$gesamt = $duration * $anzsli;
$sz = $slidezoom / 100;
$szm = ($slidezoom + 100) / 200;

/*
$document = JFactory::getDocument();

if($titelfont != 'none') :
  $document->addStyleSheet('http://fonts.googleapis.com/css?family='.$titelfont);
endif;
*/

$style = '
body { background: none; background-color: '.$hgfarbe.'; }
ul.extrobgs-slideshow { margin: 0px; list-style: none; }
.extrobgs-slideshow, .extrobgs-slideshow:after { position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; }
.extrobgs-slideshow { z-index: -1; }
.extrobgs-slideshow:after { content: ""; background: transparent url("'.$pattern.'") repeat top left; z-index: 0; }
.extrobgs-slideshow li span { width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; color: transparent; background-size: cover; background-position: 50% 50%; background-repeat: none; opacity: 0; z-index: 0; -webkit-backface-visibility: hidden; -webkit-animation: imageAnimation '.$gesamt.'s linear infinite 0s; -moz-animation: imageAnimation '.$gesamt.'s linear infinite 0s; -o-animation: imageAnimation '.$gesamt.'s linear infinite 0s; -ms-animation: imageAnimation '.$gesamt.'s linear infinite 0s; animation: imageAnimation '.$gesamt.'s linear infinite 0s; }
.extrobgs-slideshow li div { z-index: 1000; position: absolute; bottom: 50px; left: 0px; width: 100%; text-align: center; opacity: 0; -webkit-animation: titleAnimation '.$gesamt.'s linear infinite 0s; -moz-animation: titleAnimation '.$gesamt.'s linear infinite 0s; -o-animation: titleAnimation '.$gesamt.'s linear infinite 0s; -ms-animation: titleAnimation '.$gesamt.'s linear infinite 0s; animation: titleAnimation '.$gesamt.'s linear infinite 0s; }
.extrobgs-slideshow li div h3 { font-family: "'.$titelfont.'", Arial, sans-serif; font-size: '.$titelfontsize.'px; padding: 0 30px; line-height: '.$titelfontsize.'px; color: '.$titlecolor.'; opacity: 0.8; }
';

for($x=0;$x<count($si);++$x) {
  $style .= '.extrobgs-slideshow li:nth-child('.($x+1).') span { background-image: url("'.$si[$x].'"); -webkit-animation-delay: '.($x * $duration).'s; -moz-animation-delay: '.($x * $duration).'s; -o-animation-delay: '.($x * $duration).'s; -ms-animation-delay: '.($x * $duration).'s; animation-delay: '.($x * $duration).'s; }
';
if($x == 0) { $style .= '.extrobgs-slideshow li:first-child span { background-image: url("'.$si[$x].'"); opacity: 1; }'; }
}

$y = 25;
if(count($si) > 0) { $y = 150 / count($si); }
if($y < 25 ) { $y = 25; }

for($x=0;$x<count($si);++$x) {
  $style .= '.extrobgs-slideshow li:nth-child('.($x+1).') div { -webkit-animation-delay: '.($x * $duration).'s; -moz-animation-delay: '.($x * $duration).'s; -o-animation-delay: '.($x * $duration).'s; -ms-animation-delay: '.($x * $duration).'s; animation-delay: '.($x * $duration).'s; }
';
}

$style .= '
@-webkit-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -webkit-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -webkit-transform: scale('.$szm.');
	    -webkit-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -webkit-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	'.$y.'% {
	    opacity: 0;
	    -webkit-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	100% { opacity: 0 }
}
@-moz-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -moz-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -moz-transform: scale('.$szm.');
	    -moz-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -moz-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	'.$y.'% {
	    opacity: 0;
	    -moz-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	100% { opacity: 0 }
}
@-o-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -o-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -o-transform: scale('.$szm.');
	    -o-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -o-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	'.$y.'% {
	    opacity: 0;
	    -o-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	100% { opacity: 0 }
}
@-ms-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -ms-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -ms-transform: scale('.$szm.');
	    -ms-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -ms-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	'.$y.'% {
	    opacity: 0;
	    -ms-transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	100% { opacity: 0 }
}
@keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    transform: scale('.$szm.');
	    animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	'.$y.'% {
	    opacity: 0;
	    transform: scale('.$sz.') rotate('.$slidetilt.'deg);
	}
	100% { opacity: 0 }
}

@media screen and (max-width: 1140px) { 
	.cb-slideshow li div h3 { font-size: 100px }
}
@media screen and (max-width: 600px) { 
	.cb-slideshow li div h3 { font-size: 50px }
}
';

switch($titeleffekt):
  case 1:
    $style .= '
@-webkit-keyframes titleAnimation { 
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-moz-keyframes titleAnimation { 
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-o-keyframes titleAnimation { 
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-ms-keyframes titleAnimation { 
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@keyframes titleAnimation { 
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
';
    break;
  case 2:
    $style .= '
@-webkit-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -webkit-transform: translateX(200px);
	}
	8% {
	    opacity: 1;
	    -webkit-transform: translateX(0px);
	}
	17% {
	    opacity: 1;
	    -webkit-transform: translateX(0px);
	}
	19% {
	    opacity: 0;
	    -webkit-transform: translateX(-400px);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-moz-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -moz-transform: translateX(200px);
	}
	8% {
	    opacity: 1;
	    -moz-transform: translateX(0px);
	}
	17% {
	    opacity: 1;
	    -moz-transform: translateX(0px);
	}
	19% {
	    opacity: 0;
	    -moz-transform: translateX(-400px);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-o-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -o-transform: translateX(200px);
	}
	8% {
	    opacity: 1;
	    -o-transform: translateX(0px);
	}
	17% {
	    opacity: 1;
	    -o-transform: translateX(0px);
	}
	19% {
	    opacity: 0;
	    -o-transform: translateX(-400px);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-ms-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -ms-transform: translateX(200px);
	}
	8% {
	    opacity: 1;
	    -ms-transform: translateX(0px);
	}
	17% {
	    opacity: 1;
	    -ms-transform: translateX(0px);
	}
	19% {
	    opacity: 0;
	    -ms-transform: translateX(-400px);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    transform: translateX(200px);
	}
	8% {
	    opacity: 1;
	    transform: translateX(0px);
	}
	17% {
	    opacity: 1;
	    transform: translateX(0px);
	}
	19% {
	    opacity: 0;
	    transform: translateX(-400px);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
';
    break;
  case 3:
    $style .= '
@-webkit-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -webkit-transform: translateY(-300%);
	}
	8% {
	    opacity: 1;
	    -webkit-transform: translateY(0%);
	}
	17% {
	    opacity: 1;
	    -webkit-transform: translateY(0%);
	}
	19% {
	    opacity: 0;
	    -webkit-transform: translateY(100%);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-moz-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -moz-transform: translateY(-300%);
	}
	8% {
	    opacity: 1;
	    -moz-transform: translateY(0%);
	}
	17% {
	    opacity: 1;
	    -moz-transform: translateY(0%);
	}
	19% {
	    opacity: 0;
	    -moz-transform: translateY(100%);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-o-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -o-transform: translateY(-300%);
	}
	8% {
	    opacity: 1;
	    -o-transform: translateY(0%);
	}
	17% {
	    opacity: 1;
	    -o-transform: translateY(0%);
	}
	19% {
	    opacity: 0;
	    -o-transform: translateY(100%);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@-ms-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -ms-transform: translateY(-300%);
	}
	8% {
	    opacity: 1;
	    -ms-transform: translateY(0%);
	}
	17% {
	    opacity: 1;
	    -ms-transform: translateY(0%);
	}
	19% {
	    opacity: 0;
	    -ms-transform: translateY(100%);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}
@keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    transform: translateY(-300%);
	}
	8% {
	    opacity: 1;
	    transform: translateY(0%);
	}
	17% {
	    opacity: 1;
	    transform: translateY(0%);
	}
	19% {
	    opacity: 0;
	    transform: translateY(100%);
	}
	25% { opacity: 0 }
	100% { opacity: 0 }
}

';
    break;
  case 4:
    $style .= '
@-webkit-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -webkit-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -webkit-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -webkit-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -webkit-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-moz-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -moz-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -moz-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -moz-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -moz-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-o-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -o-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -o-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -o-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -o-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-ms-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -ms-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -ms-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -ms-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -webkit-transform: scale(10);
	}
	100% { opacity: 0 }
}
@keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    transform: scale(10);
	}
	100% { opacity: 0 }
}
';
    break;
endswitch;


$document->addStyleDeclaration($style);

$customstyle = '<!--[if lte IE 9]>' . "\n";
$customstyle .= '<style type="text/css"> .extrobgs-slideshow li div { display: none; } </style>' . "\n";
$customstyle .= '<![endif]-->' . "\n";
$document->addCustomTag($customstyle);


echo '<ul class="extrobgs-slideshow no-cssanimations '.$classuffix.'">';

for($x=0;$x < count($si);++$x) {
  echo '<li><span>&nbsp; </span><div><h3>'.$st[$x].'</h3></div></li>';
}

echo '</ul>';

} else {

if ($duration < 3 ) { $duration = 3; }
$capt_dur = 0; $capt_dur = $duration - 1.5;
$cssfile = 'modules/mod_extrobgs/mod_extrobgs.css';
$document->addStyleSheet($cssfile);
$cssfile = 'modules/mod_extrobgs/effects.css';
$document->addStyleSheet($cssfile);
$style = '
.extrobgs-fullwidth { background: none repeat scroll 0 0 '.$hgfarbe.'; }
.extrobgs ul.itemwrap:after { content: ""; background: transparent url("'.$pattern.'") repeat top left; z-index: 1001; position: fixed;  }
div.extrobgs.extrobgs-fullwidth ul.itemwrap li div h3 { font-family: "'.$titelfont.'", Arial, sans-serif; font-size: '.$titelfontsize.'px; line-height: '.$titelfontsize.'px; color: '.$titlecolor.'; }
div.extrobgs.extrobgs-fullwidth ul.itemwrap li.current div { -webkit-animation: titleAnimation'.$titeleffekt.' '.$capt_dur.'s linear; animation: titleAnimation'.$titeleffekt.' '.$capt_dur.'s linear; }
';
for($x=0;$x<count($si);++$x) {
	$style .= 'div.extrobgs.extrobgs-fullwidth ul.itemwrap li:nth-child('.($x+1).') span { background-image: url("'.$si[$x].'"); } ';
}
$document->addStyleDeclaration($style);
echo '<div id="extrobgs" class="extrobgs extrobgs-fullwidth fx'.$effekt.'">';
echo '<ul class="itemwrap">';
for($x=0;$x < count($si);++$x) {
	$klasse = '';
	if($x == 0) { $klasse = 'current'; }
  echo '<li class="'.$klasse.'"><span></span><div><h3>'.$st[$x].'</h3></div></li>';
}
echo '</ul>';
echo '</div>';
}
$script = "
jQuery(document).ready(function(){
  ads = ".(count($si) - 1).";
  ds = 0;
  ns = 0;

setInterval(function(){
  
  setTimeout(function(){
    ns = ds + 1;
    if(ns > ads) { ns = 0; }
    jQuery('ul.itemwrap li').eq(ds).addClass('navOutNext');
    jQuery('ul.itemwrap li').eq(ns).addClass('navInNext');
  },0);

  setTimeout(function(){
    jQuery('ul.itemwrap li').eq(ds).removeClass('current');
    jQuery('ul.itemwrap li').eq(ns).addClass('current');
    jQuery('ul.itemwrap li').eq(ds).removeClass('navOutNext');
    jQuery('ul.itemwrap li').eq(ns).removeClass('navInNext');
    ds = ns;
  },1500);

  if(ns > ads) { ns = 0; }

},".($duration * 1000)."
);
 
});
";
$document->addScriptDeclaration($script);
?>
