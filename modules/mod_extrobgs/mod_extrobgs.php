<?php

/*
# -------------------------------------------------------------------------
# mod_extrobgs - eXtro Background Slider Module (E.R.B.S.E.)
# -------------------------------------------------------------------------
# author     eXtro-media.de
# copyright  Copyright (C) 2012 eXtro-media.de. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
# Websites:  http://www.eXtro-media.de
# Technical Support:  Forum - http://www.extro-media.de/en/forum.html
# -------------------------------------------------------------------------
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// Include the syndicate functions only once
// require_once( dirname(__FILE__).'/helper.php' );
 
require( JModuleHelper::getLayoutPath( 'mod_extrobgs' ) );
?>