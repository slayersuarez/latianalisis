/**
* Compartir View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RegistroView = {};

	// Extends my object from Backbone events
	RegistroView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'change #departamento': 'loadCities',

			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'savePersona',
					'editPersona',
					'saveEmpresa',
					'editEmpresa',
					'loadCities'
				);

				// this.usuarios = new RegistroModel();
				// this.loadCities();
				this.showAccordion();

			}

			/**
			* Validate form persona
			*
			*/
			
		,	savePersona: function(e) {

				e.preventDefault();

				var data = utilities.formToJson( '#member-registration-persona' )
				,	required = [ 
							'nombre'
		        		,	'apellidos'
		        		,	'email'
		        		,	'password'
		        		,	'cedula'
		        		,	'direccion'
		        		,	'telefono'
		        		,	'celular'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				
				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.email != data.confirmar_email )
					return utilities.showNotification( 'error', 'Los correos no coinciden', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( data.departamento == '' )
					return utilities.showNotification( 'error', 'Seleccione un departamento', 0 );

				if( data.ciudad == '' )
					return utilities.showNotification( 'error', 'Seleccione una ciudad', 0 );

				if( ! utilities.justNumbers( data.ciudad ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				if( data.terminos == '2' )
					return utilities.showNotification( 'error', 'Debe aceptar las condiciones legales.', 0 );

				this.usuarios.set( data );

				this.usuarios.savePersona( this.onCompleteSaveUser, this.onError );

			}

			/**
			* onComplete save persona
			*
			*/

		,	onCompleteSaveUser: function( data ){

				if ( data.status == 500 )
					return utilities.showNotification( 'error', data.message , 0 );

				if ( data.status == 300 )
					return utilities.showNotification( 'error', data.message , 0 );

				utilities.showNotification( 'error', data.message , 1000, function(){

					window.location.href = '../index.php/component/users/?view=login';

				});
			}

				/**
			* Validate form persona
			*
			*/
			
		,	saveEmpresa: function(e) {

				e.preventDefault();

				var data = utilities.formToJson( '#member-registration-empresa' )
				,	required = [ 
							'empresa'
		        		,	'contacto'
		        		,	'email'
		        		,	'password'
		        		,	'nit'
		        		,	'direccion'
		        		,	'telefono'
		        		,	'celular'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				
				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.email != data.confirmar_email )
					return utilities.showNotification( 'error', 'Los correos no coinciden', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( data.departamento == '' )
					return utilities.showNotification( 'error', 'Seleccione un departamento', 0 );

				if( data.ciudad == '' )
					return utilities.showNotification( 'error', 'Seleccione una ciudad', 0 );

				if( ! utilities.justNumbers( data.ciudad ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				if( data.terminos == '2' )
					return utilities.showNotification( 'error', 'Debe aceptar las condiciones legales.', 0 );

				this.usuarios.set( data );

				this.usuarios.saveEmpresa( this.onCompleteSaveUser, this.onError );

			}

			/**
			* onComplete save persona
			*
			*/

		,	onCompleteSaveUser: function( data ){

				if ( data.status == 500 )
					return utilities.showNotification( 'error', data.message , 0 );

				if ( data.status == 300 )
					return utilities.showNotification( 'error', data.message , 0 );

				utilities.showNotification( 'error', data.message , 1000, function(){

					window.location.href = '../index.php/component/users/?view=login';

				});
			}

		,	editPersona: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#member-edit-persona' )
				,	required = [ 
							'nombre'
		        		,	'apellidos'
		        		,	'email'
		        		,	'cedula'
		        		,	'direccion'
		        		,	'telefono'
		        		,	'celular'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				
				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( data.departamento == '' )
					return utilities.showNotification( 'error', 'Seleccione un departamento', 0 );

				if( data.ciudad == '' )
					return utilities.showNotification( 'error', 'Seleccione una ciudad', 0 );

				if( ! utilities.justNumbers( data.ciudad ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				this.usuarios.set( data );

				this.usuarios.editPersona( this.onCompleteEditUser, this.onError );


			}

		,	editEmpresa: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#member-edit-empresa' )
				,	required = [ 
							'empresa'
		        		,	'contacto'
		        		,	'email'
		        		,	'nit'
		        		,	'direccion'
		        		,	'telefono'
		        		,	'celular'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				
				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( data.departamento == '' )
					return utilities.showNotification( 'error', 'Seleccione un departamento', 0 );

				if( data.ciudad == '' )
					return utilities.showNotification( 'error', 'Seleccione una ciudad', 0 );

				if( ! utilities.justNumbers( data.ciudad ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				this.usuarios.set( data );

				this.usuarios.editEmpresa( this.onCompleteEditUser, this.onError );


			}


			/**
			* onComplete edit persona
			*
			*/

		,	onCompleteEditUser: function( data ){

				if ( data.status == 500 )
					return utilities.showNotification( 'error', data.message , 0 );

				if ( data.status == 300 )
					return utilities.showNotification( 'error', data.message , 0 );

				utilities.showNotification( 'error', data.message , 1000, function(){

					window.location.reload();

				});
			}

		,	showPersona: function(){

				window.location.href = "usuarios"

			}

		,	showEmpresa: function(){

				window.location.href = "usuarios?layout=empresa"

			}

		,	loadCities: function(){

				var id = $('#departamento').val();
				var option = '';

				if ( $('#city').length > 0) {
					var ciudad = $('#city').val();
				}

				this.usuarios.getCiudades( id, function( data ){

					$.each(data.municipios, function(index, val) {

						var selected = '';

						if ( ciudad == val.id ){
							selected = 'selected';
						}
						
						option = '<option '+selected+' value="'+val.id+'">'+val.nombre+'</option>';

						$('#ciudad').append(option);
					});

					$('#ciudad').removeAttr('disabled');


				}, this.onError);

			}

		,	showAccordion: function(){

				$('#accordion').accordion();
			}
			

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RegistroView = new RegistroView();

})( jQuery, this, this.document, this.Misc, undefined );