/**
* Estados de Resultados Historicos View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var ResultadosHistoricosView = {};

	// Extends my object from Backbone events
	ResultadosHistoricosView = Backbone.View.extend({

			el: $( '.content-resultados-historicos' )

		,	events: {
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'renderTable',
					'renderTableBalanceHistoricoActivo',
					'renderTableBalanceHistoricoPasivo',
					'renderTableBalanceHistoricoPatrimonio'
				);

				// this.renderHistoricos();
				this.renderTable();
				this.renderTableBalanceHistoricoActivo();
				this.renderTableBalanceHistoricoPasivo();
				this.renderTableBalanceHistoricoPatrimonio();
				// google.load("visualization", "1", {packages:["corechart"]});
				// google.setOnLoadCallback( this.renderTortaHistoricosExt );

			}

		,	tableSelector: '.resultados-historicos'

		,	tablaSelecionadaActivo: '#balance-resultado-historico-activo'

		,	tablaSelecionadaPasivo: '#balance-resultado-historico-pasivo'

		,	tablaSelecionadaPatrimonio: '#balance-resultado-historico-patrimonio'


		// ,	renderHistoricos: function(){

		// 		var html = new EJS({url: url + '/js/templates/resultadoshistoricos.ejs'}).render({});

		// 		$( '#wrapper-table-historicos' ).html( html );
		// 	}

			/**
			* Build and render the table with JSON data
			*
			*
			*/
		,	renderTable: function(){

				var _this = this;

				$( this.tableSelector ).dynatable({
				 	dataset: {
						ajax: true,
						ajaxUrl: url + 'js/test/resultadoshistoricos.json',
						ajaxOnLoad: true,
						records: []
					},

					features: {
						paginate: false,
						search: false,
						recordCount: false,
						perPageSelect: false
					}
				});

			}

			/**
			* Build and render the table with JSON data
			*
			*
			*/
		,	renderTableBalanceHistoricoActivo: function(){

				var _this = this;

				$( this.tablaSelecionadaActivo ).dynatable({
				 	dataset: {
						ajax: true,
						ajaxUrl: url + 'js/test/balancehistoricoactivo.json',
						ajaxOnLoad: true,
						records: []
					},

					features: {
						paginate: false,
						search: false,
						recordCount: false,
						perPageSelect: false
					}
				});

			}

		,	renderTableBalanceHistoricoPasivo: function(){

				var _this = this;

				$( this.tablaSelecionadaPasivo ).dynatable({
				 	dataset: {
						ajax: true,
						ajaxUrl: url + 'js/test/balancehistoricopasivo.json',
						ajaxOnLoad: true,
						records: []
					},

					features: {
						paginate: false,
						search: false,
						recordCount: false,
						perPageSelect: false
					}
				});

			}	

		,	renderTableBalanceHistoricoPatrimonio: function(){

				var _this = this;

				$( this.tablaSelecionadaPatrimonio).dynatable({
				 	dataset: {
						ajax: true,
						ajaxUrl: url + 'js/test/balancehistoricopatrimonio.json',
						ajaxOnLoad: true,
						records: []
					},

					features: {
						paginate: false,
						search: false,
						recordCount: false,
						perPageSelect: false
					}
				});

			}						

		// ,	renderTortaHistoricosExt: function(){

		//         var data = google.visualization.arrayToDataTable([
		//           ['Task', 'Hours per Day'],
		//           ['Work',     11],
		//           ['Eat',      2],
		//           ['Commute',  2],
		//           ['Watch TV', 2],
		//           ['Sleep',    7]
		//         ]);

		//         var options = {
		//           title: 'Composición de el balance'
		//         };

		//         var chart = new google.visualization.PieChart(document.getElementById('composicion_balance_torta'));

		//         chart.draw(data, options);

		// 	}	

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.ResultadosHistoricosView = new ResultadosHistoricosView();

})( jQuery, this, this.document, this.Misc, undefined );