/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var ResultadosHistoricosExtView = {};

	// Extends my object from Backbone events
	ResultadosHistoricosExtView = Backbone.View.extend({

			el: $( '.content-resultados-historicos' )

		,	events: {
				
			}

		,	view: this

		,	initialize: function(){

				// _.bindAll(
				// 	this,

				// );

				this.renderHistoricos();


				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback( this.renderTortaHistoricosExt );
				google.setOnLoadCallback( this.renderEvolucionVentasExt );
				google.setOnLoadCallback( this.renderEvolucionMargenBrExt );
				google.setOnLoadCallback( this.renderEvolucionMargenOpExt );
				google.setOnLoadCallback( this.renderEvolucionMargenNetoExt );
				

			}

		,	renderHistoricos: function(){

				var html = new EJS({url: url + '/js/templates/resultadoshistoricos.ejs'}).render({});

				$( '#wrapper-table-rhistoricos' ).html( html );
			}

		,	renderTortaHistoricosExt: function(){

		        var data = google.visualization.arrayToDataTable([
		          ['Task', 'Hours per Day'],
		          ['Work',     11],
		          ['Eat',      2],
		          ['Commute',  2],
		          ['Watch TV', 2],
		          ['Sleep',    7]
		        ]);

		        var options = {
		          title: 'Composición de el balance'
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('composicion_balance_torta'));

		        chart.draw(data, options);

			}	

		,	renderEvolucionVentasExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución de Ventas',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('evolucion_ventas_columas'));

				chart.draw(data, options);

			}	

		,	renderEvolucionMargenBrExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución de Ventas',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.LineChart(document.getElementById('evolucion-margen-br'));

				chart.draw(data, options);

			}		

			,	renderEvolucionMargenOpExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  00],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución Margen Op.',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.LineChart(document.getElementById('evolucion-margen-op'));

				chart.draw(data, options);

			}			

			,	renderEvolucionMargenNetoExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución de Ventas',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.LineChart(document.getElementById('evolucion-margen-nt'));

				chart.draw(data, options);

			}									

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.ResultadosHistoricosExtView = new ResultadosHistoricosExtView();

})( jQuery, this, this.document, this.Misc, undefined );