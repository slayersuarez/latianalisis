/**
* Información Corporativa View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información corporativa de una empresa consultada
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var InfoCorporativaView = {};

	// Extends my object from Backbone events
	InfoCorporativaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'renderVentasChart'
				);


				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback( this.renderVentasChart );

				
				google.setOnLoadCallback( this.renderVentasChartPasivoActivo );


      			google.setOnLoadCallback( this.renderEvolucionRiegoChart );

				
      			google.setOnLoadCallback( this.renderEndeudamientoChart ); 


			}

			/**
			* Draw Ventas Chart
			*
			*/
		,	renderVentasChart: function(){

				var data = google.visualization.arrayToDataTable([
					['Año', 'ventas', {role: 'style'}],
					['2010',  1000, '#F3CC24'],
					['2011',  1170, '#F3CC24'],
					['2012',  660, '#F3CC24'],
					['2013',  1030, '#F3CC24']
				]);

				var options = {
					title: 'Ventas',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('ventas-chart'));

				chart.draw(data, options);

			}

		,	renderVentasChartPasivoActivo: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales', 'Expenses'],
				    ['2004',  1000,      400],
				    ['2005',  1170,      460],
				    ['2006',  660,       1120],
				    ['2007',  1030,      540]
				  ]);

				var options = {
					title: '',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('pasivo-activo-chart'));

				chart.draw(data, options);

			}
		,	renderEvolucionRiegoChart: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolucion de Riego',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};


				var chart = new google.visualization.LineChart(document.getElementById('evolucion-riesgo'));

				chart.draw(data, options);

			}

		,	renderEndeudamientoChart: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Endeudamiento',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('endeudamiento-chart'));

				chart.draw(data, options);

			}			

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.InfoCorporativaView = new InfoCorporativaView();

})( jQuery, this, this.document, this.Misc, undefined );