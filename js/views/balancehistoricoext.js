/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var BalanceHistoricoExtView = {};

	// Extends my object from Backbone events
	BalanceHistoricoExtView = Backbone.View.extend({

			el: $( '.content-resultados-historicos' )

		,	events: {
				
			}

		,	view: this

		,	initialize: function(){

				// _.bindAll(
				// 	this,

				// );

				this.renderBalanceHistoricosExt();


				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback( this.renderComposicionBalanceExt );
				google.setOnLoadCallback( this.renderComposicionActivoExt );
				google.setOnLoadCallback( this.renderEvolucionActivoExt );
				google.setOnLoadCallback( this.renderComposicionPasivoExt );
				google.setOnLoadCallback( this.renderEvolucionPasivoExt );
				google.setOnLoadCallback( this.renderEvolucionActivoExt );
				google.setOnLoadCallback( this.renderComposicionPatrimonioExt );
				google.setOnLoadCallback( this.renderEvolucionPatrimonioExt );								

			}

		,	renderBalanceHistoricosExt: function(){

				var html = new EJS({url: url + '/js/templates/balancehistoricoext.ejs'}).render({});

				$( '#wrapper-table-balancehistorico-ext' ).html( html );
			}

		,	renderComposicionBalanceExt: function(){

		        var data = google.visualization.arrayToDataTable([
		          ['Task', 'Hours per Day'],
		          ['Work',     11],
		          ['Eat',      2],
		          ['Commute',  2],
		          ['Watch TV', 2],
		          ['Sleep',    7]
		        ]);

		        var options = {
		          title: 'Composición de el balance'
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('composicion-balance'));

		        chart.draw(data, options);

			}	

		,	renderComposicionActivoExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Composición Activo',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.PieChart(document.getElementById('composicion-activo'));

				chart.draw(data, options);

			}	

		,	renderEvolucionActivoExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución Activo',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('evolucion-activo'));

				chart.draw(data, options);

			}		

		,	renderComposicionPasivoExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Composición Pasivo',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.PieChart(document.getElementById('composicion-pasivo'));

				chart.draw(data, options);

			}	

		,	renderEvolucionPasivoExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución Pasivo',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('evolucion-pasivo'));

				chart.draw(data, options);

			}	

		,	renderComposicionPatrimonioExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Composición Patrimonio',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.PieChart(document.getElementById('composicion-patrimonio'));

				chart.draw(data, options);

			}	

		,	renderEvolucionPatrimonioExt: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Evolución Patrimonio',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('evolucion-patrimonio'));

				chart.draw(data, options);

			}							
								

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.BalanceHistoricoExtView = new BalanceHistoricoExtView();

})( jQuery, this, this.document, this.Misc, undefined );