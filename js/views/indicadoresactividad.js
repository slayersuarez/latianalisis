/**
* Estados de Indicadores actividad
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var IndicadoresActividadView = {};

	// Extends my object from Backbone events
	IndicadoresActividadView = Backbone.View.extend({

			el: $( '.content-resultados-historicos' )

		,	events: {
				
			}

		,	view: this

		,	initialize: function(){

				this.renderIndicadores();

				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback( this.renderIndicadorRotcxc );
				google.setOnLoadCallback( this.renderIndicadorRotinv );
				google.setOnLoadCallback( this.renderIndicadorRotcxp );
				google.setOnLoadCallback( this.renderIndicadorRotinv );				
				google.setOnLoadCallback( this.renderIndicadorCapitalTrabajo );
				google.setOnLoadCallback( this.renderIndicadorCicloEfect );

				google.setOnLoadCallback( this.renderIndicadorRentabilidadPatrimonio );
				google.setOnLoadCallback( this.renderIndicadorMargenOperacional );			
				google.setOnLoadCallback( this.renderIndicadorRoic );

				google.setOnLoadCallback( this.renderIndicadorMargenBruto );
				google.setOnLoadCallback( this.renderIndicadorRentabilidadActivos );			
				google.setOnLoadCallback( this.renderIndicadorMargenNeto );	

				google.setOnLoadCallback( this.renderIndicadorEndeudamientoTotal );
				google.setOnLoadCallback( this.renderIndicadorLevargeFinanciero );
				google.setOnLoadCallback( this.renderIndicadorUtilidadOperacional );
				google.setOnLoadCallback( this.renderIndicadoEndeudamientoFinanciero );			
				google.setOnLoadCallback( this.renderIndicadorObligacionesFinancieras );							
			}

			/**
			* Con esta funcion vamos a mostrar la tabla de indicadores de actividades
			*
			*/
		,	renderIndicadores: function(){


				var html = new EJS({url: url + '/js/templates/indicadoresactividad.ejs'}).render({});

				$( '#wrapper-indicadores-actividad' ).html( html );
			}

		,	renderIndicadorRotcxc: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Rot. CxC',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-rotcxc'));

				chart.draw(data, options);

			}			

		,	renderIndicadorRotinv: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Rot.-Inv',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-rotinv'));

				chart.draw(data, options);

			}

		,	renderIndicadorRotcxp: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Rot. CxP',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-rotcxp'));

				chart.draw(data, options);

			}									


		,	renderIndicadorCapitalTrabajo: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Capital de Trabajo',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.ColumnChart(document.getElementById('indicador-capital-trabajo'));

				chart.draw(data, options);

			}

		,	renderIndicadorCicloEfect: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Ciclo Efectivo',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-ciclo-efect'));

				chart.draw(data, options);

			}									

		,	renderIndicadorRentabilidadPatrimonio: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Rentabilidad Sobre Patrimonio',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-rentabilidad-patrimonio'));

				chart.draw(data, options);

			}

		,	renderIndicadorRentabilidadActivos: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Rentabilidad Sobre Activos',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-rentabilidad-activos'));

				chart.draw(data, options);

			}

		,	renderIndicadorRoic: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Roic',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-roic'));

				chart.draw(data, options);

			}						

		,	renderIndicadorMargenBruto: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Margen Bruto',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-margen-bruto'));

				chart.draw(data, options);

			}

		,	renderIndicadorMargenOperacional: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Margen Operacional',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-margen-operacional'));

				chart.draw(data, options);

			}

		,	renderIndicadorMargenNeto: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Margen Neto',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-margen-neto'));

				chart.draw(data, options);

			}				



/// asjkdfhasfdhaosdfhas

		,	renderIndicadorEndeudamientoTotal: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Indicador Endeudamiento Total',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-endeudamiento-total'));

				chart.draw(data, options);

			}

		,	renderIndicadoEndeudamientoFinanciero: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Endeudamiento Financiero',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-endeudamiento-financiero'));

				chart.draw(data, options);

			}						

		,	renderIndicadorUtilidadOperacional: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Utilidad Operacional',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-utilidad-operacional'));

				chart.draw(data, options);

			}

		,	renderIndicadorLevargeFinanciero: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Levarege Financiero',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-levarege-financiero'));

				chart.draw(data, options);

			}

		,	renderIndicadorObligacionesFinancieras: function(){

				 var data = google.visualization.arrayToDataTable([
				    ['Year', 'Sales'],
				    ['2004',  1000],
				    ['2005',  1170],
				    ['2006',  660],
				    ['2007',  1030]
				  ]);

				var options = {
					title: 'Roic',
					curveType: 'function',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 400,
					orientation:'horizontal',
					legend: { position: 'bottom' }
				};

				var chart = new google.visualization.LineChart(document.getElementById('indicador-obl-finaciera'));

				chart.draw(data, options);

			}												
			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.IndicadoresActividadView = new IndicadoresActividadView();

})( jQuery, this, this.document, this.Misc, undefined );