 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="menu_component">

	<ul class="tabs">
		<li class="active"> <a href="#">Buscador de Empresas</a> </li>
		<li>  <a href="index.php/productos/?layout=buscador_avanzado"> Buscador Avanzado </a> </li>
		<li> <a href="index.php/productos/?layout=buscador_sectorial"> Análisis Sectorial</a> </li>
		<li> <a href="index.php/component/users/profile?layout=edit">Mi Cuenta</a> </li>			
	</ul>
</div>

<div id="content_component">
	<div class="tites_component"><h3 class="analisis">Buscador de Empresas</h3></div>

	<div class="buscadores">
		<div class="desc_search"><strong>NIT</strong><span class="claro">(Sin dígito de verificación )</span> </div>
	    <div class="buscador">
	        <input type="text" name="search" />
	        <label>Buscar</label>
	    </div>
	</div>
</div>

<div class="content_result_table">

	<table class="titles">
	  <tr>
	    <td class="tl_empresa"><span class="n_empresa">Nombre Empresa</span></td>
	    <td class="tl_nit"> <span class="n_nit">NIT</span></td>
	    <td class="tg-031e"></td>
	  </tr>

	<table class="results">
	  <tr>
	    <td class="n_company"> <span class="name_company"> Lorem ipsum dolor</span></td>
	    <td class="i_company"> <span class="nit_company"> 25917499327-3 </td>
	    <td class="icn_detalle"> <span class="ver_reporte"> <a href="index.php/productos/?layout=resultado_general">Ver Reporte</a> </span> </td>
	  </tr>
	  <tr>
	    <td class="n_company"> <span class="name_company"> Lorem ipsum dolor</span></td>
	    <td class="i_company"> <span class="nit_company"> 25917499327-3 </td>
	    <td class="icn_detalle"> <span class="ver_reporte"> <a href="index.php/productos/?layout=resultado_general">Ver Reporte</a> </span> </td>
	  </tr>
	  <tr>
	    <td class="n_company"> <span class="name_company"> Lorem ipsum dolor</span></td>
	    <td class="i_company"> <span class="nit_company"> 25917499327-3 </td>
	    <td class="icn_detalle"> <span class="ver_reporte"> <a href="index.php/productos/?layout=resultado_general">Ver Reporte</a> </span> </td>
	  </tr>
	</table>

</div>

