 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_analisis
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>

<div class="content_acordeon content-resultados-historicos">
	<div class="name_nit">
		<div class="name_detail"> DISTRIBUIDORA EL EJEMPLO S.A.S</div>
		<div class="nit_detail"> <span class="nit_azul">Nit:</span> <span class="nit_detail">860000000</span> </div>
	</div>

	<div class="subtitles_component">
		<h4>Balance General Historicos</h4>
	</div>

	<div class="tablas_cien">
		<table class="resultados-historicos azul">
			<thead>
				<th>CUENTAS</th>
				<th>2010</th>
				<th>2011</th>
				<th>2012</th>
				<th>Diciembre_2013</th> 
			</thead>
		</table>	

		<table id="balance-resultado-historico-activo" class="azul">
			<thead>
				<th>ACTIVO</th>
				<th>2010</th>
				<th>2011</th>
				<th>2012</th>
				<th>Diciembre_2013</th> 
			</thead>
		</table>

		<table id="balance-resultado-historico-pasivo" class="azul">
			<thead>
				<th>PASIVO</th>
				<th>2010</th>
				<th>2011</th>
				<th>2012</th>
				<th>Diciembre_2013</th> 
			</thead>
		</table>

		<table id="balance-resultado-historico-patrimonio" class="azul">
			<thead>
				<th>PATRIMONIO</th>
				<th>2010</th>
				<th>2011</th>
				<th>2012</th>
				<th>Diciembre_2013</th> 
			</thead>
		</table>		

		<table class="total resultados-historicos_total azul">
			<tbody>
				<tr>
					<td>TOTAL PASIVO Y PATRIMONIO</td>
					<td>20210</td>
					<td>156.011</td>
					<td>0</td>
					<td>556.000</td> 
				</tr>			
			</tbody>
		</table>

		<table class="resumen_estadistica resultado_historico_resumen">
			<tbody>
				<tr>
					<td>Utilidades Distribuidos</td>
					<td>0</td>
					<td>1</td>
					<td>0</td>
					<td>0</td>
				</tr>
				<tr>
					<td>Utilidades Capitalizadas </td>
					<td>0</td>
					<td>0</td>
					<td>1</td>
					<td>0</td>
				</tr>	
				<tr>
					<td>Aumentos de Capital</td>
					<td>0</td>
					<td>0</td>
					<td>1</td>
					<td>0</td>
				</tr>	
				<tr>
					<td>Diferencia en balance</td>
					<td>0</td>
					<td>1</td>
					<td>0</td>
					<td>0</td>
				</tr>												
			</tbody>
		</table>		
	</div>

	<div class="clear"></div>
</div>

