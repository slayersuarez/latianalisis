 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_analisis
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>

<div class="content_acordeon content-resultados-historicos">
	<div class="name_nit">
		<div class="name_detail"> DISTRIBUIDORA EL EJEMPLO S.A.S</div>
		<div class="nit_detail"> <span class="nit_azul">Nit:</span> <span class="nit_detail">860000000</span> </div>
	</div>

	<div class="subtitles_component">
		<h4>Estado resultados Histórico</h4>
	</div>

	<div id="wrapper-table-rhistoricos">

	</div>

	<div class="subtitles_component">
		<h4>Balance General Histórico</h4>
	</div>

	<div id="wrapper-table-balance-historicos">

	</div>

	<div id="wrapper-table-balancehistorico-ext">

	</div>


	<div class="clear"></div>
</div>

