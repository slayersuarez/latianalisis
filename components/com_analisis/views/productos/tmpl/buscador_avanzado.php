 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="menu_component">

	<ul class="tabs">
		<li>  <a href="index.php/productos">Buscador de Empresas</a> </li>
		<li class="active">  <a href="index.php/productos/?layout=buscador_avanzado"> Buscador Avanzado </a> </li>
		<li> <a href="index.php/productos/?layout=analisis_sectorial"> Análisis Sectorial</a> </li>
		<li> <a href="index.php/component/users/profile?layout=edit">Mi Cuenta</a> </li>		
	</ul>
</div>

<div id="content_component">
	<div class="tites_component"><h3 class="analisis">Buscador Avanzado</h3></div>

	<div class="wrapper_search">

		<form class="search_advance">
			<h4>Filtros</h4>

			<table border="0" id="buscador_av" >
			<tbody>
			<tr>
				<td colspan="2">
					
					<label>Cuidad</label>
					<select name="ciudades" id="ciudades">
						<option>Seleccione</option>
						<option>Bogotá</option>
						<option>Medellin</option>
						<option>Ibague</option>
						<option>Pasto</option>
						<option>Choco</option>
						<option>Santa Marta</option>
					</select>

				</td>

				<td>			
					<label>Ingresos anuales</label>

					<select name="ingresos" id="ingresos">
						<option>Seleccione</option>
						<option> Igual a</option>
						<option> Diferente a</option>
						<option> Mayor a</option>
						<option> Menor a</option>
						<option> Mayor o igual que </option>
						<option> Menor o igual que</option>						
					</select>
				</td>

				<td>
					<input type="text" class="ingreso" placeholder="Colocar ingreso" />
				</td>

			</tr>
			<tr>
				<td colspan="2" width="50%">
					<label>Sector</label>

					<select name="sector" id="sector">
						<option>Seleccione</option>
					<!-- Estes es un auto complete que va a generar Nestiht Alejandro Suarez -->
							<option value="1">Priemera</option>
							<option value="2">Segunda</option>
							<option value="3">Tercera</option>

					</select>
				</td>

				<td colspan="2" width="50%">
					<label> Riesgo </label>
					<select name="riesgo" id="riesgo">
						<option>Seleccione</option>
						<option> Riesgo Bajo </option>
						<option> Riesgo Medio Bajo </option>
						<option> Riesgo Medio </option>
						<option> Riesgo Medio Alto </option>
						<option> Riesgo Alto </option>					
					</select>
				</td>

			</tr>
			<tr>
				<td>			
					<label>Sugerido de crédito</label>

					<select name="s_credito" id="s_credito">
						<option>Seleccione</option>
						<option> Igual a </option>
						<option> Diferente a </option>
						<option> Mayor a </option>
						<option> Menor a </option>
						<option> Mayor o igual que </option>
						<option> Menor o igual que</option>						
					</select>
				</td>

				<td>
					<input type="text" class="s_credito"  />
				</td>

				<td>			
					<label>No. de empleados</label>

					<select name="n_empleado" id="n_empleado">
						<option>Seleccione</option>
						<option> Igual a </option>
						<option> Diferente a </option>
						<option> Mayor a </option>
						<option> Menor a </option>
						<option> Mayor o igual que </option>
						<option> Menor o igual que</option>						
					</select>
				</td>

				<td>
					<input type="text" class="i_empleado"  />
				</td>

			</tr>
			<tr>
				<td>			
					<label>Sugerido de compra de cartera</label>

					<select name="s_cartera" id="s_cartera">
						<option>Seleccione</option>
						<option> Igual a </option>
						<option> Diferente a </option>
						<option> Mayor a </option>
						<option> Menor a </option>
						<option> Mayor o igual que </option>
						<option> Menor o igual que</option>						
					</select>
				</td>

				<td>
					<input type="text" class="i_cartera" />
				</td>

				<td>
					<label>Margen neto</label>
					<input type="text" class="margen_neto"  />
				</td>

				<td>
					<label>Endeudamiento total</label>
					<input type="text" class="endeudamiento_t"  />
				</td>

			</tr>
			<tr>
				<td>
					<label>Endeudamiento financiero</label>
					<input type="text" class="endeudamiento_f"  />
				</td>

				<td>
					<label>Utilidad operacional / Gasto financiero</label>
					<input type="text" class="utilidad_operacional"  />
				</td>

				<td colspan="2">
					<button class="avanzado_buscar">Buscar</button>
				</td>

			</tr>
			</tbody>
			</table>


		</form>

	</div>


</div>

