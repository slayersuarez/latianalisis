 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_analisis
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>

<div class="content_acordeon content-resultados-historicos">
	<div class="name_nit">
		<div class="name_detail"> DISTRIBUIDORA EL EJEMPLO S.A.S</div>
		<div class="nit_detail"> <span class="nit_azul">Nit:</span> <span class="nit_detail">860000000</span> </div>
	</div>
	<div class="descripcion_empresa">
Objeto Social: Su objeto social consiste en la explotación de la industria de alimentos en general y/o de sustancias empleadas como ingredientes en la alimentación y, especialmente de la carne y/o de la de productos agrícolas, incluido el procesamiento y utilización de subproductos de especies animales y agrícolas para la preparación de alimentos; la explotación agrícola y ganadera en sus especies mayores y menores, y los negocios directamente relacionados con dichas actividades, en especial mediante la cría, ceba, levante y engorde de ganado y su posterior sacrificio o enajenación en pie; la compra, venta, transporte, distribución, importación, exportación y comercio en general de alimentos propios y de otros fabricantes. Además, la inversión o aplicación de recursos o disponibilidades bajo cualquiera de las formas asociativas autorizadas por la ley, que tengan por objeto la explotación de cualquier actividad económica lícita aunque no se halle relacionada directamente con la producción o comercialización de alimentos; y, la realización de cualquier otra actividad económica lícita tanto en Colombia como en el exterior.
	</div>

	<div class="subtitles_component">
		<h4>Información Coprporativa</h4>
	</div>

	<div class="tablas_cien">
		<table class="datos_em inf_corp">
			<tr>
				<td class="bold"> Ciudad </td>
				<td> <span class="val"> Bogota D.C.</span> </td>
			</tr>
			<tr>
				<td class="bold"> Dirección </td>
				<td> <span class="val"> Carrera 5 No. 122- 33 </span> </td>
			</tr>
			<tr>
				<td class="bold"> Teléfono</td>
				<td> <span class="val"> 31100000</span> </td>
			</tr>
			<tr>
				<td class="bold"> Fax </td>
				<td> <span class="val"> 31100000 </span> </td>
			</tr>
			<tr>
				<td class="bold"> E-mail </td>
				<td> <span class="val"> ejemplo@ejemplo.com </span> </td>
			</tr>
			<tr>
				<td class="bold"> Sitio Web </td>
				<td> <span class="val"> www.ejemplo.com </span> </td>
			</tr>
			<tr>
				<td class="bold"> Representante legal </td>
				<td> <span class="val"> Pablo Perez </span> </td>
			</tr>
			<tr>
				<td class="bold"> Facultades del Rep. Legal </td>
				<td> <span class="val">Ilimitadas</span> </td>
			</tr>	
			<tr>
				<td class="bold"> Número de Empleados </td>
				<td> <span class="val"> 38 </span> </td>
			</tr>	
			<tr>
				<td class="bold"> Imp_Exp </td>
				<td> <span class="val"> Importa, Exporta </span> </td>
			</tr>
			<tr>
				<td colspan="2" class="l_fila"> <span class="exporta">Exportaciones. Valor FOB dólares</span> <span class="valor_corporativa"> 56.765.125 </span> </td>
			</tr>																											
		</table>

		<table class="datos_em2 inf_corp">
			<tr>
				<td class="bold"> CIIU </td>
				<td> <span class="val"> D1750</span> </td>
			</tr>
			<tr>
				<td class="bold"> Matrícula </td>
				<td> <span class="val"> 05343 del 01-01-01 </span> </td>
			</tr>
			<tr>
				<td class="bold"> Organización Jurídica</td>
				<td> <span class="val"> SAS EJEMPLO</span> </td>
			</tr>
			<tr>
				<td class="bold"> Categoría </td>
				<td> <span class="val"> 0 </span> </td>
			</tr>
			<tr>
				<td class="bold">Estado Matrícula</td>
				<td> <span class="val"> Vigente </span> </td>
			</tr>
			<tr>
				<td class="bold"> Fecha Matrícula/Constitución</td>
				<td> <span class="val"> 01/01/2001 </span> </td>
			</tr>
			<tr>
				<td class="bold"> Fecha Última Renovación </td>
				<td> <span class="val">  01/01/2014 </span> </td>
			</tr>
			<tr>
				<td class="bold"> Ultimo Año Renovado </td>
				<td> <span class="val"> Indefinido</span> </td>
			</tr>	
			<tr>
				<td class="bold"> Vigencia </td>
				<td> <span class="val"> Común </span> </td>
			</tr>	
			<tr>
				<td colspan="2" class="l_fila"> <span class="exporta">Importaciones.Valor CIF dólares</span> <span class="valor_corporativa"> 56.765.125 </span> </td>
			</tr>	
		</table>
	</div>	
	<div class="clear"></div>
	<span class="info_reportes">
No presenta reportes de embargo, liquidación, disolución, reestructuración, ley 1116, intervención económica o registros de demandas.
	</span>

	<div class="subtitles_component">
		<h4>Concepto de Riesgo y sugerido de crédito</h4>
	</div>

	<div class="tablas_cien">
		<table class="concepto_riesgo amarilla">
			<tr>
				<td>
					<span class="nivel_riesgo">RIESGO MEDIO ALTO</span> 
				</td>
			</tr>
			<tr>
				<td>
					<span class="tipo_evolucion"> Evolución Inestable </span> 
				</td>
			</tr>
			<tr>
				<td>
					<span class="recomendacion"> No se recomienda cupo por su nivel de deuda financiera. Se recomienda un análisis mayor para compra de cartera por: </span>
				</td>
			</tr>
			<tr>
				<td>
					$<span class="valor_concepto_riesgo">4.693.539.511</span>	
				</td>
			</tr>
		</table>
	</div>	

	<div class="subtitles_component">
		<h4>Evolución del Riesgo</h4>
	</div>

	<div class="tablas_cien">
		<table class="evolucion_riesgo amarilla">
			<tr>
				<td class="anio_evolucion">2010</td>
				<td>
					<span class="nivel_riesgo_evolucion">RIESGO MEDIO ALTO</span> 
				</td>
			</tr>
			<tr>
				<td class="anio_evolucion">2011</td>
				<td>
					<span class="nivel_riesgo_evolucion">RIESGO MEDIO ALTO</span> 
				</td>
			</tr>
			<tr>
				<td class="anio_evolucion">2012</td>
				<td>
					<span class="nivel_riesgo_evolucion">RIESGO ALTO</span> 
				</td>
			</tr>
			<tr>
				<td class="anio_evolucion">2013</td>
				<td>
					<span class="nivel_riesgo_evolucion">RIESGO MEDIO ALTO</span> 
				</td>
			</tr>
		</table>
	</div>

	<div class="charts-info-general">

		<div id="ventas-chart"></div>

		<div id="evolucion-riesgo"></div>

		<div id="pasivo-activo-chart"></div>

		<div id="endeudamiento-chart"></div>
	</div>	

</div>



