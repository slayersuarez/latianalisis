 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="menu_component">

	<ul class="tabs">
		<li>  <a href="index.php/productos">Buscador de Empresas</a> </li>
		<li>  <a href="index.php/productos/?layout=buscador_avanzado"> Buscador Avanzado </a> </li>
		<li> <a href="index.php/productos/?layout=analisis_sectorial"> Análisis Sectorial</a> </li>
		<li> <a href="index.php/component/users/profile?layout=edit">Mi Cuenta</a> </li>			
	</ul>
</div>

	<div id="accordion">
		<h3>Información Corporativa</h3>
		<div>
			<?php include("informacion_corporativa.php") ?>
		</div>
		<h3>Estdo de resultados histórico</h3>
		<div>
			<?php include("estado_historico.php") ?>
		</div>
		<h3>Balance general histórico</h3>
		<div>
			<?php include("balance_general_historico.php") ?>
		</div>
		<h3>Indicadores financieros históricos</h3>
		<div>
			<?php include("indicadores_financieros_h.php") ?>
		</div>
	</div>

</div>

