 <?php
/**
 * @package		Joomla.Site
 * @subpackage	com_analisis
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>

<div class="content_acordeon content-resultados-historicos">
	<div class="name_nit">
		<div class="name_detail"> DISTRIBUIDORA EL EJEMPLO S.A.S</div>
		<div class="nit_detail"> <span class="nit_azul">Nit:</span> <span class="nit_detail">860000000</span> </div>
	</div>

	<div class="subtitles_component">
		<h4>Indicadores financieros historicos</h4>
	</div>

	<div id="wrapper-indicadores-actividad">

	</div>

	<div class="clear"></div>
</div>

