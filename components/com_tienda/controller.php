<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Anuncios Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_si
 */
class TiendaController extends JControllerLegacy {
	/**
	 * @var		string	The default view.
	 * @since	1.6
	 */
	protected $default_view = 'detalle';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display( $cachable = false, $urlparams = false) {
		
		$render = JRequest::getVar('render', '');
		
		if(! empty( $render ) ){
			
			$metodo = "render".$render;
			$this->$metodo();
			return;
		}
		
		$this->renderDefault();

		parent::display( $cachable, $urlparams );

		return $this;
	}
	
	private function renderDefault(){
	
		
		$view =& $this->getView( $default_view, 'html' );
		
		// Call the model
		$model =& $this->getModel( 'object' );
		
		// Get all the Objects
		$object = $model->getObjects();
		
		// Assign objects as reference to the view
		$view->assignRef( 'objects', $objects );
		
	
	}
	
	
	public function renderEdit(){
		
		$id = JRequest::getVar( 'id' );
	
		$view =& $this->getView( 'object', 'html' );
		
		// Call the model
		$model =& $this->getModel( 'object' );
		
		// Get the object
		$object = $model->getObject( $id );
		
		// Assign objects as reference to the view
		$view->assignRef( 'object', $object );
		
		// Display the view
		$view->display("new");
		
	}
}