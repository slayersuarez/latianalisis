<!DOCTYPE HTML>

<html lang="es">

    <head>
        <jdoc:include type="head" />
        <meta charset="utf-8"/>

        <!-- Estos son los estilos -->
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=analisis">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">

        <!--Estas son las fuentes-->
        <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,800' rel='stylesheet' type='text/css'>
        <!--SCRIPTS-->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
            var x;
            x=jQuery(document);
            x.ready(inicio);
            function inicio(){
                var ancho= window.innerWidth;
                var margen= (1900-ancho)/2;
                var x;
                x=jQuery(".banner");
                x.css("margin-left","-"+margen+"px");
            }
             jQuery( document ).ready( function( e ){


               if (document.documentElement.clientWidth < 480) { 
                   document.querySelector("meta[name=viewport]").setAttribute(
                         'content', 
                         'width=device-width, initial-scale=0.1, maximum-scale=0.1, user-scalable=1');

                   return;
               }

               if (document.documentElement.clientWidth < 1024) { 
                   document.querySelector("meta[name=viewport]").setAttribute(
                         'content', 
                         'width=device-width, initial-scale=0.7, maximum-scale=0.7, user-scalable=1');

                   return;
               }
           });
        </script>
    </head>

    <body onresize="inicio()">
        <header>    
            <div class="header-top">
                <nav>
                    <div class="menu1">
                        <jdoc:include type="modules" name="menu1" style="xhtml"/>
                    </div>
                </nav>
                <div class="login">
                    <jdoc:include type="modules" name="login" style="xhtml"/>
                </div>
            </div>
            <div class="header-bottom"> 
                <div class="logo">
                    <iframe src="html5/logo.html" alt="Logo"/></iframe>
                    <!-- <image src="images/logo.png" /> -->
                </div>  
                <div class="botones-secciones">
                    <jdoc:include type="modules" name="botones-secciones" style="xhtml"/>
                </div>      
            </div>
        </header>
        <main>
            <div class="content-all">
                    <jdoc:include type="message" />
                    <jdoc:include type="component" />
            </div>
        </main> 

        <footer>
            <div class="contenedor-footer">
                <div class="info">
                    <jdoc:include type="modules" name="info" style="xhtml"/>
                </div>
                <div class="copy">
                    <span class="sainet">
                        <a target="_blank" href="http://www.creandopaginasweb.com">
                            By  <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logoverde.png">
                        </a>
                    </span>
                </div>
            </div>
        </footer>
        <script type="text/javascript">

            var url = "<?php echo JFactory::getURI()->root(); ?>";
        </script>
        <script type="text/javascript" src="js/load-scripts.php"></script>
    </body>
</html>