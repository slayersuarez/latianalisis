<!DOCTYPE HTML>

<html>

	<head>
		<jdoc:include type="head" />
		<meta charset="utf-8"/>

		<!-- Estos son los estilos -->
		<link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">

		<!--Estas son las fuentes-->
		<link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,800' rel='stylesheet' type='text/css'>
		<!--SCRIPTS-->
	</head>

	<body onresize="inicio()">
		<div class="banner">
			<jdoc:include type="modules" name="banner" style="xhtml"/>
		</div>
		<header>	
			<div class="header-top">
				<nav>
					<div class="menu1">
						<jdoc:include type="modules" name="menu1" style="xhtml"/>
					</div>
				</nav>
				<div class="login">
					<jdoc:include type="modules" name="login" style="xhtml"/>
				</div>
			</div>
			<div class="header-bottom">	
				<div class="logo">
					<iframe src="html5/logo.html" alt="Logo"/></iframe>
					<!-- <image src="images/logo.png" /> -->
				</div> 	
				<div class="botones-secciones">
					<jdoc:include type="modules" name="botones-secciones" style="xhtml"/>
				</div> 		
			</div>
		</header>
			<div class="texto">
				<jdoc:include type="modules" name="texto" style="xhtml"/>
			</div>
		
		<main>
			<div class="content-all">
				<div class="financiacion">
					<jdoc:include type="modules" name="financiacion" style="xhtml"/>
				</div>
			</div>
		</main>	

		<footer>
			<div class="contenedor-footer">
				<div class="info">
					<jdoc:include type="modules" name="info" style="xhtml"/>
				</div>
				<div class="copy">
					<span class="sainet">
						<a target="_blank" href="http://www.creandopaginasweb.com">
							By  <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logoverde.png">
						</a>
					</span>
				</div>
			</div>
		</footer>
	</body>
</html>