(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 230,
	height: 180,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"images/Group.png", id:"Group"},
		{src:"images/logoazul.png", id:"logoazul"},
		{src:"images/sombra.png", id:"sombra"}
	]
};



// symbols:



(lib.Group = function() {
	this.initialize(img.Group);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,284,232);


(lib.logoazul = function() {
	this.initialize(img.logoazul);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,176,129);


(lib.sombra = function() {
	this.initialize(img.sombra);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,176,129);


(lib.Símbolo16 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.Group();
	this.instance.setTransform(0,0,0.995,1.026);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,282.5,238);


(lib.Símbolo15 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.sombra();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,176,129);


(lib.Símbolo14 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.logoazul();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,176,129);


(lib.Símbolo13 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("ABQhBIg5AAQgFAAAAgFQAAgTgBgFQgBgKgFgEQgEgEgHAAQgRABgBATQgCAYARASQAHAKAWASQAHAGAOANQAMAJAIAJQAMAQAGASQACAGAAAIQACAngFAXQgJAngmAHQglAJgkgKQgngKgIgpQgEgWACgfQAAgDAEAAIA6AAQAEAAgBAFQgBAJACASQACAOALADQAGADALgEQAIgEABgNQABgMgCgNQgDgNgKgLQgGgIgMgLIgVgRQgagWgLgOQgOgRgBgWQgCghAFgWQAJgnAogJQAhgJAgAHQAnAIAKAoQAGAYgCAeQAAAEgFAAg");
	this.shape.setTransform(8.7,17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AgkCqQgngKgIgpQgEgWACgfQAAgDAEAAIA6AAQAEAAgBAFQgBAJACASQACAOALADQAGADALgEQAIgEABgNQABgMgCgNQgDgNgKgLQgGgIgMgLIgVgRQgagWgLgOQgOgRgBgWQgCghAFgWQAJgnAogJQAhgJAgAHQAnAIAKAoQAGAYgCAeQAAAEgFAAIg5AAQgFAAAAgFQAAgTgBgFQgBgKgFgEQgEgEgHAAQgRABgBATQgCAYARASQAHAKAWASIAVATQAMAJAIAJQAMAQAGASQACAGAAAIQACAngFAXQgJAngmAHQgSAEgSAAQgSAAgTgFg");
	this.shape_1.setTransform(8.7,17.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,19.5,37.1);


(lib.Símbolo12 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AghCjIAAlEQAAgGAGAAIA4AAQAFAAAAAFIAAFGQAAAEgFAAIg5AAQgFAAAAgFg");
	this.shape.setTransform(3.4,16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AgcCoQgFAAAAgFIAAlEQAAgGAGAAIA4AAQAFAAAAAFIAAFGQAAAEgFAAg");
	this.shape_1.setTransform(3.4,16.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.9,35.7);


(lib.Símbolo11 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("ABQhCIg5AAQgFAAAAgFQAAgSgCgJQgCgPgOABQgQABgCAPQgFAdAYAYQAqAkANAKQAbAbADAbQAEAogIAeQgJAkgjAIQglAIgigIQgrgLgIgrQgCgMAAgnQAAgDAEAAIA6AAQAEAAAAAEQAAAUABAFQADAZAWgFQAKgCACgJQAHgigSgVQgEgFgTgSQgmgegOgRQgRgUAAgVQgCgjADgSQAJgrAqgJQAhgHAfAGQApAIAKArQADAQgBAiQAAAEgEAAg");
	this.shape.setTransform(8.7,17.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AghCrQgrgLgIgrQgCgMAAgnQAAgDAEAAIA6AAQAEAAAAAEQAAAUABAFQADAZAWgFQAKgCACgJQAHgigSgVQgEgFgTgSQgmgegOgRQgRgUAAgVQgCgjADgSQAJgrAqgJQAhgHAfAGQApAIAKArQADAQgBAiQAAAEgEAAIg5AAQgFAAAAgFQAAgSgCgJQgCgPgOABQgQABgCAPQgFAdAYAYIA3AuQAbAbADAbQAEAogIAeQgJAkgjAIQgTAEgSAAQgQAAgSgEg");
	this.shape_1.setTransform(8.7,17.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,19.5,37.1);


(lib.Símbolo10 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AAiiiIAAFFQAAAFgGAAIg4AAQgFAAAAgEIAAlGQAAgFAFAAIA5AAQAFAAAAAFg");
	this.shape.setTransform(3.4,16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AgcCoQgFAAABgEIAAlGQAAgFAEAAIA5AAQAEAAAAAFIAAFFQABAFgGAAg");
	this.shape_1.setTransform(3.4,16.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.8,35.7);


(lib.Símbolo9 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AhFCjIAAlEQAAgEABgBQABgBAEAAIA2AAQAGAAABABQABABAAAGIAAEDQAAAFABABQAAAAADAAIA+AAQAFAAAAAFQgBAbABAaQAAAEgFAAIiBAAQgFAAAAgFg");
	this.shape.setTransform(7,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AhACoQgFAAAAgFIAAlEQAAgFABAAQABgBAEAAIA2AAQAGAAABABQABABAAAGIAAEEQAAAEABABIADAAIA+AAQAFABAAAEIAAA1QAAAEgFAAg");
	this.shape_1.setTransform(7,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16.1,38);


(lib.Símbolo8 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("ABmClIg5lHQAAgFgFAAIhNAAQgHAAgBAGIg4FFQgBAEAFAAIA+AAQADAAABgDIAGg0QABgEADAAIArAAQADAAAAADIAHA0QAAAEAEAAIA/AAQADAAAAgDgAAIAwIgUAAQgDAAgBgBQgBgBAAgDIARiJQAAgIAAgEIAQCQQABAHgBACQgBABgHAAg");
	this.shape.setTransform(10.2,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AAkCoQgEAAAAgEIgHg0QAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAIgrAAQgDAAgBAEIgGA0QAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAIg+AAQgFAAABgEIA4lFQABgGAHAAIBNAAQAFAAAAAFIA5FHQAAABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAgAAAheIgRCJQAAABAAABQAAAAAAABQAAAAABABQAAAAAAAAIAEABIAUAAQAHAAABgBQABgCgBgHIgQiQQAAAEAAAIg");
	this.shape_1.setTransform(10.2,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,22.5,38.2);


(lib.Símbolo7 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AgwhjIABEGQAAAFgFAAIgzAAQgEAAAAgEIAAlGQAAgDABgBQABgBADAAIBRAAQAFAAABAFQAlCXATBRIAHAdQABgBAAgDIgBh7QAAhZgBgsQAAgGAGAAIAzAAQAEAAAAAFIAAFGQAAAEgEAAIhTAAQgEAAgBgEQgXhagUhVg");
	this.shape.setTransform(10.9,16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AAVCoQgEAAgBgEQgXhagUhVIgVhYIABEGQAAAFgFAAIgzAAQgEAAAAgEIAAlGQAAgBAAgBQAAAAAAgBQAAAAAAgBQABAAAAAAIAEgBIBRAAQAFAAABAFQAlCXATBRIAHAdIABgEIgBh7IgBiFQAAgGAGAAIAzAAQAEAAAAAFIAAFGQAAAEgEAAg");
	this.shape_1.setTransform(10.9,16.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-3.2,23.8,40.2);


(lib.Símbolo6 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("ABmCjIg4lGQgBgEgEAAIhQAAQgFAAAAAEIg4FGQgBADABABQABABADAAIA8AAQAFAAAAgFIAHgyQAAgFAFAAQATABAUgBQAGAAAAAGQACARAEAhQABAEAEAAIA9AAQAEAAAAgFgAANAvIgZAAQgFAAABgEIAQiTIAAAAIARCTQABAEgFAAg");
	this.shape.setTransform(10.2,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("ABiCoIg9AAQgEAAgBgEIgGgyQAAgGgGAAQgUABgTgBQgFAAAAAFIgHAyQAAAFgFAAIg8AAIgEgBQAAAAAAAAQgBgBAAAAQAAgBAAAAQAAgBABgBIA4lGQAAgEAFAAIBQAAQAEAAABAEIA4FGQAAAFgEAAIAAAAgAgQArQgBAEAFAAIAZAAQAFAAgBgEIgRiTIAAAAg");
	this.shape_1.setTransform(10.2,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,22.4,38);


(lib.Símbolo5 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AAiiiIAAFFQAAAFgGAAIg4AAQgEAAAAgEIAAlHQAAgEAEAAIA5AAQAFAAAAAFg");
	this.shape.setTransform(3.4,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AgcCoQgEAAgBgEIAAlHQABgEAEAAIA4AAQAGAAAAAFIAAFFQgBAFgFAAg");
	this.shape_1.setTransform(3.4,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.8,35.7);


(lib.Símbolo4 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AAhhkIAAEHQAAADgBABQAAABgEAAIg2AAQgFAAAAgFIAAkFQAAgGgBAAQAAgBgGAAIgmAAQgFAAAAgEIAAg2QAAgEAEAAICbAAQAEAAAAAEIAAA2QAAAEgEAAIgoAAQgFAAAAAFg");
	this.shape.setTransform(8.3,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AgfCjIAAkFIgBgGIgGgBIgmAAQgFAAAAgEIAAg2QAAgEAEAAICbAAQAEAAAAAEIAAA2QAAAEgEAAIgoAAQgFAAAAAFIAAEHIgBAEQAAAAAAAAQAAABgBAAQAAAAgBAAQgBAAgBAAIg2AAIgBAAQgEAAAAgFg");
	this.shape_1.setTransform(8.3,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,18.6,35.7);


(lib.Símbolo3 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("ABmCkIg5lHQAAgEgFAAIhPAAQgFAAAAAEIg4FHQgBAEAEAAIA9AAQAFAAAAgEIAGgwQABgGABAAQABgBAFAAIAkAAQAFAAABABQABAAAAAFQAAACAGAvQAAAEAFAAIA9AAQAEAAAAgEgAAOAwIgbAAQgEAAABgEIAQiVIAAAAIARCVQABAEgEAAg");
	this.shape.setTransform(10.2,16.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AAlCoQgFAAAAgEIgGgxQAAgFgBAAQgBgBgFAAIgkAAQgFAAgBABQgBABgBAFIgGAwQAAAEgFAAIg9AAQgEAAABgEIA4lHQAAgEAFAAIBPAAQAFAAAAAEIA5FHQAAAEgEAAgAgQArQgBAFAEAAIAbAAQAEgBgBgDIgRiVIAAAAg");
	this.shape_1.setTransform(10.2,16.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,22.4,35.7);


(lib.Símbolo2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.8).p("AhFCjIAAlEQAAgGAFAAIA6AAQAFAAAAAGIAAEFQAAAGABAAQAAABAEAAIA8AAQAGAAAAAFQgBAaABAaQAAAEgFAAIiCAAQgEAAAAgFg");
	this.shape.setTransform(7.1,16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898989").s().p("AhACoQgFAAAAgFIAAlEQAAgGAGAAIA5AAQAFAAABAGIAAEFIAAAGIAEABIA8AAQAGAAAAAFIAAA0QAAAEgFAAg");
	this.shape_1.setTransform(7.1,16.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16.1,38.1);


(lib.Interpolación6 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#038CD1").s().p("Am+LBIAA2BIN9AAIAAWBg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-44.7,-70.5,89.5,141.1);


(lib.Interpolación5 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#038CD1").s().p("Am+LBIAA2BIN9AAIAAWBg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-44.7,-70.5,89.5,141.1);


// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_249 = function() {
		this.gotoAndPlay(136)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(249).call(this.frame_249).wait(1));

	// S
	this.instance = new lib.Símbolo13();
	this.instance.setTransform(212.8,154,0.08,0.08,0,0,0,8.8,17.6);
	this.instance.alpha = 0.211;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(97).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},7).wait(146));

	// I
	this.instance_1 = new lib.Símbolo12();
	this.instance_1.setTransform(196.6,154,0.176,0.176,0,0,0,3.4,16.7);
	this.instance_1.alpha = 0.211;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(90).to({_off:false},0).to({regY:16.9,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(153));

	// S
	this.instance_2 = new lib.Símbolo11();
	this.instance_2.setTransform(180.5,154,0.069,0.069,0,0,0,8.7,17.4);
	this.instance_2.alpha = 0.211;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(83).to({_off:false},0).to({regY:17.6,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(160));

	// I
	this.instance_3 = new lib.Símbolo10();
	this.instance_3.setTransform(164.3,154,0.099,0.099,0,0,0,3.6,16.7);
	this.instance_3.alpha = 0.211;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(76).to({_off:false},0).to({regX:3.4,regY:16.9,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(167));

	// L
	this.instance_4 = new lib.Símbolo9();
	this.instance_4.setTransform(150.4,155.4,0.086,0.086,0,0,0,7,18.1);
	this.instance_4.alpha = 0.211;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(69).to({_off:false},0).to({regY:18.3,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(174));

	// A
	this.instance_5 = new lib.Símbolo8();
	this.instance_5.setTransform(130.8,155.5,0.062,0.062,0,0,0,10.5,18.5);
	this.instance_5.alpha = 0.211;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(62).to({_off:false},0).to({regY:18.4,scaleX:1,scaleY:1,x:130.9,alpha:1},7,cjs.Ease.get(1)).wait(181));

	// N
	this.instance_6 = new lib.Símbolo7();
	this.instance_6.setTransform(107.1,154,0.078,0.078,0,0,0,10.8,16.6);
	this.instance_6.alpha = 0.211;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(55).to({_off:false},0).to({regY:16.9,scaleX:1,scaleY:1,x:107,alpha:1},7,cjs.Ease.get(1)).wait(188));

	// A
	this.instance_7 = new lib.Símbolo6();
	this.instance_7.setTransform(83.4,155.4,0.066,0.066,0,0,0,9.9,18.3);
	this.instance_7.alpha = 0.211;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(48).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(195));

	// I
	this.instance_8 = new lib.Símbolo5();
	this.instance_8.setTransform(67.5,154,0.101,0.101,0,0,0,3.5,16.9);
	this.instance_8.alpha = 0.211;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(41).to({_off:false},0).to({regX:3.4,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(202));

	// T
	this.instance_9 = new lib.Símbolo4();
	this.instance_9.setTransform(52.8,154,0.072,0.072,0,0,0,8.3,16.6);
	this.instance_9.alpha = 0.211;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(34).to({_off:false},0).to({regX:8.2,regY:16.9,scaleX:1,scaleY:1,x:52.7,alpha:1},7,cjs.Ease.get(1)).wait(209));

	// A
	this.instance_10 = new lib.Símbolo3();
	this.instance_10.setTransform(35.9,154,0.071,0.071,0,0,0,10.6,16.9);
	this.instance_10.alpha = 0.211;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(27).to({_off:false},0).to({regX:10.2,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).wait(216));

	// L
	this.instance_11 = new lib.Símbolo2();
	this.instance_11.setTransform(17.6,155.5,0.112,0.112,0,0,0,7.2,18.2);
	this.instance_11.alpha = 0.211;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(20).to({_off:false},0).to({regX:7,regY:18.4,scaleX:1,scaleY:1,x:17.5,alpha:1},7,cjs.Ease.get(1)).wait(223));

	// FlashAICB (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_135 = new cjs.Graphics().p("AHLIGQjeggjtAAQjsAAjdAgQjXAfixA6QBcg4B+gqQB/gqCWgYIAAiGIAugaIAACZIAqgFIAAk9IAugaIAAFSIAqgDIAAu4IAugbIAAPQIAqgCIAAvmIAvgaIAAP+IAqAAIAAv+ICiBnIAAI5IBfA0IAAB7IBYAwIAACYQCZAYCCArQCBAqBeA5Qixg6jWgfg");
	var mask_graphics_249 = new cjs.Graphics().p("AHLIGQjeggjtAAQjsAAjdAgQjXAfixA6QBcg4B+gqQB/gqCWgYIAAiGIAugaIAACZIAqgFIAAk9IAugaIAAFSIAqgDIAAu4IAugbIAAPQIAqgCIAAvmIAvgaIAAP+IAqAAIAAv+ICiBnIAAI5IBfA0IAAB7IBYAwIAACYQCZAYCCArQCBAqBeA5Qixg6jWgfg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(135).to({graphics:mask_graphics_135,x:116.3,y:71.1}).wait(114).to({graphics:mask_graphics_249,x:116.3,y:71.1}).wait(1));

	// brillo
	this.instance_12 = new lib.Interpolación5("synched",0);
	this.instance_12.setTransform(-43.9,66.5);
	this.instance_12._off = true;

	this.instance_13 = new lib.Interpolación6("synched",0);
	this.instance_13.setTransform(162.1,66.5);

	this.instance_12.mask = this.instance_13.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},135).to({state:[{t:this.instance_13}]},10).to({state:[{t:this.instance_13}]},104).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(135).to({_off:false},0).to({_off:true,x:162.1},10,cjs.Ease.get(1)).wait(105));

	// edificio
	this.instance_14 = new lib.Símbolo14();
	this.instance_14.setTransform(115.9,130.9,1,0.072,0,0,0,88,64.2);
	this.instance_14.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({regY:64.5,scaleY:1,y:71,alpha:1},14,cjs.Ease.get(1)).wait(236));

	// sombra
	this.instance_15 = new lib.Símbolo15();
	this.instance_15.setTransform(116,69.5,1,1,0,0,0,88,64.5);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(14).to({_off:false},0).to({x:118.2,y:71},6).wait(230));

	// FlashAICB
	this.instance_16 = new lib.Símbolo16();
	this.instance_16.setTransform(117.7,90,0.16,0.16,0,0,0,141.2,119);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(104).to({_off:false},0).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(141));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.9,216.3,176,9.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;